# What is Gamey?
Gamey is a Sega Game Gear emulator! <br  />
It's main purpose is just to emulate the Z80 chip inside the Game Gear as a
learning exercise with the actual roms being a second priority.
