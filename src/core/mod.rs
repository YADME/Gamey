// Our internal crates
mod registers;
// Our use cases

pub struct Core {
    registers: registers::Registers,
}

impl Core {
    pub fn new() -> Self {
        Core {
            registers: registers::Registers::new(),
        }
    }
}
