pub struct Registers {
    // Special Registers
    pub pc: u16,
    pub sp: u16,

    // Index Registers
    pub ix: u16,
    pub iy: u16,

    // General Registers
    pub af: (u8, u8),
    pub bc: (u8, u8),
    pub de: (u8, u8),
    pub hl: (u8, u8),

    // Flags
    flags: Flags,

    // Alternate Registers
    pub _af: (u8, u8),
    pub _bc: (u8, u8),
    pub _de: (u8, u8),
    pub _hl: (u8, u8),

    // Other Registers
    pub i: u8,
    pub r: u8,
}

enum Flags {
    C,  // Carry Flag
    N,  // Add/Subtract Flag
    PV, // Parity/Overflow Flag
    H,  // Half Carry Flag
    Z,  // Zero Flag
    S,  // Sign Flag
    X,  // Not Used
}

impl Registers {
    pub fn new() -> Self {
        Registers {
            pc: 0,
            sp: 0,
            ix: 0,
            iy: 0,
            
            af: (0, 0),
            bc: (0, 0),
            de: (0, 0),
            hl: (0, 0),

            _af: (0, 0),
            _bc: (0, 0),
            _de: (0, 0),
            _hl: (0, 0),
            
            flags: Flags::X,
            
            i: 0,
            r: 0,
        }
    }
}
