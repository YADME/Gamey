struct Emitter {
    buffer: u8,         // The pointer to the allocated code generation buffer
    buffer_size: u16,   // The size of the buffer in bytes
    buffer_pointer: u16,
}