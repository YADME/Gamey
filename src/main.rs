// Our internal crates
mod core;
// Our use cases
use std::env;
use std::path;

// Our struct Emulator containing the core, ppu, etc
struct Emulator {
    core: core::Core,
}

impl Emulator {
    pub fn new() -> Self {
        Emulator {
            core: core::Core::new(),
        }
    }
}

fn main() {
    // Create a variable containing our Emulator
    let emulator = Emulator::new();
    
    println!("Gamey is running");
}
